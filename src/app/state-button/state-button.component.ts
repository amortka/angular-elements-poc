import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-state-button',
  templateUrl: './state-button.component.html',
  styleUrls: ['./state-button.component.scss']
})
export class StateButtonComponent implements OnInit {

  @Input()
  clicks: number;

  @Output()
  clicked = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.clicks = this.clicks || 0;
  }

  onClick() {
    this.clicks++;
    this.clicked.emit(this.clicks);
  }
}
