import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { MockLocationStrategy } from '@angular/common/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './common/material.module';
import { AppComponent } from './app.component';
import { StateButtonComponent } from './state-button/state-button.component';

export const webComponents = [
  StateButtonComponent,
  AppComponent,
];


@NgModule({
  declarations: [
    ...webComponents,
  ],
  entryComponents: [
    ...webComponents
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: MockLocationStrategy }
  ],
})
export class AppModule {
  ngDoBootstrap() { }
}
